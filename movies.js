const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}

/*
 * NOTE: For all questions, the returned data must contain all the movie information including its name.

Q1. Find all the movies with total earnings more than $500M. 
Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
Q.3 Find all movies of the actor "Leonardo Dicaprio".
Q.4 Sort movies (based on IMDB rating)
    if IMDB ratings are same, compare totalEarning as the secondary metric.
Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
    drama > sci-fi > adventure > thriller > crime

*/




// Q1. Find all the movies with total earnings more than $500M. 

let moviesEarningMoreThan500 = Object.entries(favouritesMovies).filter(items => {
    if ((parseInt(items[1]["totalEarnings"].replace("$", ""))) > 500) {
        return true;
    }
});
console.log("Movies more than $500M earning : ");
console.log(moviesEarningMoreThan500);




//Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.



let oscarNominatedMoviesMoreThan3 = Object.entries(favouritesMovies).filter(items => {
    if ((parseInt(items[1]["totalEarnings"].slice(1))) > 500 && items[1]["oscarNominations"] > 3) {
        return true;
    }
});
console.log("Movies who got more than 3 oscarNominations and also totalEarning are more than $500M : ");
console.log(oscarNominatedMoviesMoreThan3);



// Q.3 Find all movies of the actor "Leonardo Dicaprio".


let movieActorLeonardo = Object.entries(favouritesMovies).reduce((acc, current) => {
    if (current[1]["actors"].includes('Leonardo Dicaprio')) {
        acc.push(current);
    }
    return acc;

}, []);

console.log("All movies of the actor Leonardo Dicaprio : ");
console.log(movieActorLeonardo);





// Q.4 Sort movies (based on IMDB rating)
//     if IMDB ratings are same, compare totalEarning as the secondary metric.

let sortedbyIMDBratings = Object.entries(favouritesMovies).sort((a,b)=>{
    if(a[1]["imdbRating"] == b[1]["imdbRating"]){
        if(a[1]["totalEarnings"] > b[1]["totalEarnings"]){
            return 1;
        }
     } 
    
    else if(a[1]["imdbRating"] > b[1]["imdbRating"]){
            return 1;
     }
      
     else{
        return -1;
     }
})


console.log("All movies sorted by IMDB ratings, if same IMDB then sorted by total earnings : ");
console.log(sortedbyIMDBratings);



